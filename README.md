**Time zone app**

This is a simple tool transform all time mentioned in an email to a different time zone (useful when you schedule meetings with people in different countries).


**Command Line**

1. You copy the email in which you wanna convert the times into the clipboard. 

2. Then you run the script, which  takes the number of hours by which you want to adjust the times as its sole argument, e.g.:

	```./tz.py -5```

It will change all times by -5h, i.e. 3pm to 10am and copy the changed text into your clipboard from where you can copy it back to your email client.

**Web Version**

Use `./run` to run the web version on your local machine. It's self-explanatory.
