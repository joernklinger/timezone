#!/usr/bin/env python3
import argparse
import os
from flask import Flask, request, render_template
from tz_functions import wrapper_change_tz

# Flask
app = Flask(__name__)


@app.route('/')
def my_form():
    return render_template('my-form.html')


@app.route('/', methods=['POST'])
def my_form_post():
    text = request.form['text']
    time_diff = int(request.form['range1'])
    processed_text = wrapper_change_tz(text, tz_diff=time_diff)
    return render_template('my-form.html', processed_text=processed_text)


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True, port=8080)
