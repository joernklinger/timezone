import re
import os
from dateutil import parser, tz
from datetime import datetime, timedelta
import subprocess
import string


def addToClipBoard(text):
    command = 'echo "' + text + '" | pbcopy'
    os.system(command)


def getClipboardData():
    p = subprocess.Popen(['pbpaste'], stdout=subprocess.PIPE)
    # retcode = p.wait()
    data = p.stdout.read().decode("utf-8")
    return data


def multiple_replace(dict, text):

    if dict:
        # Create a regular expression  from the dictionary keys
        regex = re.compile("(%s)" % "|".join(map(re.escape, dict.keys())))

        # For each match, look-up corresponding value in dictionary
        res = regex.sub(lambda mo: dict[mo.string[mo.start():mo.end()]], text)

    else:
        res = text

    return res


def parse_str_to_date(times):
    keep_ids = []
    dts = []
    invalid_times = []
    for i in range(len(times)):
        try:
            dts.append(parser.parse(times[i]))
            keep_ids.append(i)
        except:
            invalid_times.append(times[i])

    times = [times[i] for i in keep_ids]

    if invalid_times:
        print('invalid times: ' + str(invalid_times))

    return dts, times


def change_tz(text, tz_diff):

    # Regex for am / pm times
    regex = r'\b\d{1,2}:*\d{0,2}\s?[Aa|Pp][Mm]\b'

    # find all time strings
    times = re.findall(regex, text)

    # convert to datetime objects
    # dts = [parser.parse(time) for time in times]
    dts, times = parse_str_to_date(times)

    # change time zone
    transformed_dts = [dt + timedelta(hours=tz_diff) for dt in dts]

    # back to string
    transformed_dts_str = [dt.strftime("%I:%M%p").lower() for dt in transformed_dts]

    # remove starting zeros
    transformed_dts_str = [dt[1:] if dt.startswith('0') else dt for dt in transformed_dts_str]

    # remove leading 0
    transformed_dts_str = [dt[1:] if dt.startswith('0') else dt for dt in transformed_dts_str]

    # remove :00 for on the hour times
    transformed_dts_str = [re.sub(':00', '', dt) for dt in transformed_dts_str]

    # replace text in single pass
    replacement_dict = dict(zip(times, transformed_dts_str))

    text = multiple_replace(replacement_dict, text)

    return text


def twenty_four_to_am_pm(text):

    # Regex for 24h times
    regex = r'\b\d{1,2}:\d{2}\b'

    # Replace 24:XX by 00:XX
    text = re.sub(r'24:(\d\d)', r'00:\1', text)

    # find all time strings
    times = re.findall(regex, text)

    # convert to datetime objects
    dts = [parser.parse(t) for t in times]

    # convert back to string
    dts_am_pm_str = [dt.strftime("%I:%M%p").lower() for dt in dts]

    # replace text
    for i in range(len(times)):
        text = text.replace(times[i], dts_am_pm_str[i], 1)

    return text


def wrapper_change_tz(text, tz_diff, save_tmp=False):

    # Save original text
    original_text = text

    # Change 24h to am pm
    text = twenty_four_to_am_pm(text)

    # Change am/pm times by tz_diff
    text = change_tz(text, tz_diff)

    # Log
    print('\nOriginal Text:\n')
    print(original_text)
    print('\n\n')
    print('------------------')
    print('\n\n')
    print('New Text:\n')
    print(text)
    print('\n\n')
    print('Copied to clipboard')

    return text


def wrapper_change_tz_command_line(tz_diff, path='clipboard', save_tmp=False):
    print('Adjusting timezone of ' + path + ' by ' + str(tz_diff))

    # open and read file

    if path != 'clipboard':
        f = open(path, 'r')
        text = f.read()
    else:
        text = getClipboardData()

    # Save original text
    original_text = text

    # Change 24h to am pm
    text = twenty_four_to_am_pm(text)

    # Change am/pm times by tz_diff
    text = change_tz(text, tz_diff)

    addToClipBoard(text)

    print('\nOriginal Text:\n')
    print(original_text)
    print('\n\n')
    print('------------------')
    print('\n\n')
    print('New Text:\n')
    print(text)

    # save to tmp file
    if save_tmp:
        print('saveing output...')
        with open('tmp_time.txt', "w") as output:
            output.write(text)

    return text
