#!/usr/bin/env python3
import argparse
from tz_functions import wrapper_change_tz_command_line


if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='Convert times in text')
    argparser.add_argument('time_diff', metavar='t', type=int, help='hours by which the time shall be changed')
    args = argparser.parse_args()
    wrapper_change_tz_command_line(args.time_diff)
